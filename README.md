## Tsukuba Challenge 2017 Kanban Detection Model

つくばチャレンジ2017における看板認識をTensorFlow Object Detection APIで行うためのレポジトリです。
このレポジトリはTensorFlow Docker Workspace上で作業することを前提に作られています。
TensorFlow Docker Workspaceについては[こちらのレポジトリ](https://bitbucket.org/capygs/tensorflow-docker-ws/src)を参照してください。

notebooks`ディレクトリにこのレポジトリをcloneし、以下の手順に従ってTensorFlow Object Detection APIを使用する準備を行います。

### TensorFlow Object Detection APIの準備

`notebooks`以下にTensorFlowのmodelレポジトリをcloneします
```
cd notebooks
git clone https://github.com/tensorflow/models.git
```

### protobuf-compilerをインストール
```
sudo apt-get install -y protobuf-compiler
```

### modelsのprotobufをコンパイル
```
cd notebooks/models/research
protoc object_detection/protos/*.proto --python_out=.
```

## 学習済みモデルで推論する

### 学習結果を使用した推論

このレポジトリにはすでに学習済みの看板認識モデルが含まれています。
`models/ssd_mobilenet_v1_coco/detect_kanban.ipynb`を使用することで、学習済みのモデルを使用して看板を検出できます。

学習を自分の環境でやってみたい場合は次節を参照して下さい。

## モデルを学習する

### データセットの準備
学習に必要なデータを`data`ディレクトリに入れます。
`data/images`に学習用の画像、`data/annotations`にはPascal VOC形式のアノテーションファイルを配置します。

学習用データセットは[ここ](https://drive.google.com/open?id=0BxAJ9jXVKgBpLVE4cGtYTzNDOEU)からダウンロードできます。
画像データは試走会で[Intel ReaalSense R200](https://www.intel.co.jp/content/www/jp/ja/support/products/92256/emerging-technologies/intel-realsense-technology/intel-realsense-cameras/intel-realsense-camera-r200.html)を用いて
撮影した解像度1920x1080の画像に対して、以下のルールでラベリングしたものです。

- TsukubaChallenge 2017の看板に対して"kanban"ラベルをつける
- 前面の板全体がちょうど入る矩形を正解ラベルとする
- 看板の10%以上が隠れている場合は、範囲を予測して正解ラベルを作成し、difficultフラグをつける
- 看板の90%以上が隠れている場合はラベリングを行わない
- 正解ラベルがない画像データもアノテーションファイルを作成する

また、カメラ画像は以下のパラメータで撮影されました。

| parameter     | value | 
|:--------------|:------|
| gain          | 32    |
| exposure      | 10    |
| white_balance | 6500  |

`data/create_kanban_tf_record.ipynb`を使用して、これらのデータセットをTF Recordに変換してください。
変換時にゲイン調整とLAB空間空間の色温度調整でデータの水増しを行った上で、画像が640x360のサイズに縮小されます。

### モデルの準備
`models`には、TensorFlow Object Detection APIに使用するモデルが入っています。

#### ssd_mobilenet_v1_coco
学習はCOCOデータセットを用いた事前学習モデルをFine Tuningする形で行います。
`download_pre-trained_model.sh`を実行して、Googleによる事前学習モデルをダウンロードしてください。

### 学習

学習はDocker Containerに入った後に`/notebooks`で以下のコマンドを実行することで行うことができます。
```
python models/research/object_detection/train.py \
  --logtostderr \
  --pipeline_config=kanban/models/ssd_mobilenet_v1_coco/ssd_mobilenet_v1_coco.config \
  --train_dir=kanban/models/ssd_mobilenet_v1_coco/train
```

### 評価

評価はDocker Containerに入った後に`/notebooks`で以下のコマンドを実行することで行うことができます。
```
python models/research/object_detection/eval.py \
  --logtostderr \
  --pipeline_config=kanban/models/ssd_mobilenet_v1_coco/ssd_mobilenet_v1_coco.config \
  --checkpoint_dir=kanban/models/ssd_mobilenet_v1_coco/train
  --eval_dir=kanban/models/ssd_mobilenet_v1_coco/eval
```

### TensorBoardの起動

TensorBoardの起動はDocker Containerに入った後に`/notebooks`で以下のコマンドを実行することで行うことができます。
```
tensorboard --logdir=kanban/models/ssd_mobilenet_v1_coco
```
### 学習結果の保存

学習結果を推論で利用するために、以下のコマンドで学習結果を変換します。
```
python models/research/object_detection/export_inference_graph.py \
  --input_type image_tensor \
  --pipeline_config_path=kanban/models/ssd_mobilenet_v1_coco/ssd_mobilenet_v1_coco.config \
  --trained_checkpoint_prefix=kanban/models/ssd_mobilenet_v1_coco/train/YOUR_CHECKPOINT.ckpt-xxxxxx \
  --output_directory=kanban/models/ssd_mobilenet_v1_coco/YOUR_MODEL_NAME/
```
