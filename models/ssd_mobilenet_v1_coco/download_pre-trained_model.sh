#!/bin/bash
## Googleが公開しているCOCOデータセットで事前学習済みのデータをダウンロードする
## https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md

URL='http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v1_coco_11_06_2017.tar.gz'
wget -P ./pre-trained $URL 

cd pre-trained
tar -xzvf ssd_mobilenet_v1_coco_11_06_2017.tar.gz
rm ssd_mobilenet_v1_coco_11_06_2017.tar.gz
